//------------------------------
// testbench for mem_io_splitter
// not synthesizable
//------------------------------
`include "define.vh"
`timescale 1ns/1ns
`define TEST_DATA_SIZE 1024
`define TEST_DATA_LENGTH 10

module mem_io_splitter_tb();
   
    mem_io_splitter mem_io_splitter_dut(
    .clk  (clk),
    .rst  (rst),

    // rdy/val for nocs to splitter
    .noc1_splitter_val  (noc1_splitter_val),
    .noc2_splitter_val	(noc2_splitter_val),
			                    
    .splitter_noc1_rdy	(splitter_noc1_rdy),
    .splitter_noc2_rdy	(splitter_noc2_rdy),


    // rdy/val for splitter to nocs
    .splitter_noc3_val  (splitter_noc3_val),
    .noc3_splitter_rdy  (noc3_splitter_rdy),

    // rdy/val for splitter to mem
    .splitter_mem_val  (splitter_mem_val),
    .mem_splitter_rdy  (mem_splitter_rdy),

    // rdy/val for mem to splitter
    .mem_splitter_val  (mem_splitter_val),
    .splitter_mem_rdy  (splitter_mem_rdy),

    // rdy/val for splitter to IO
    .splitter_io_val   (splitter_io_val),
    .io_splitter_rdy   (io_splitter_rdy),

    // rdy/val for IO to splitter
    .io_splitter_val   (io_splitter_val),
    .splitter_io_rdy   (splitter_io_rdy),

    // rdy/val for splitter to noc1 sink
    .noc1sink_splitter_rdy  (noc1sink_splitter_rdy),
    .splitter_noc1sink_val  (splitter_noc1sink_val),
    
    .noc1_splitter_data  (noc1_splitter_data),
    .noc2_splitter_data  (noc2_splitter_data),

    .splitter_noc3_data  (splitter_noc3_data),
    .splitter_noc1sink_data  (splitter_noc1sink_data),

    .mem_splitter_data  (mem_splitter_data),
    .splitter_mem_data  (splitter_mem_data),

    .io_splitter_data  (io_splitter_data),
    .splitter_io_data  (splitter_io_data)      
);
     reg clk;
     reg rst;

     wire noc1_splitter_val = noc1_splitter_src_val[noc1_splitter_src_counter];
     wire noc2_splitter_val = noc2_splitter_src_val[noc2_splitter_src_counter];

     wire splitter_noc1_rdy;
     wire splitter_noc2_rdy;

     wire splitter_noc3_val;
     wire noc3_splitter_rdy = splitter_noc3_sink_rdy[splitter_noc3_sink_rdy_counter];

     wire splitter_mem_val;
     wire mem_splitter_rdy = splitter_mem_sink_rdy[splitter_mem_sink_rdy_counter];

     wire mem_splitter_val = mem_splitter_src_val[mem_splitter_src_counter];
     wire splitter_mem_rdy;

     wire splitter_io_val;
     wire io_splitter_rdy = splitter_io_sink_rdy[splitter_io_sink_rdy_counter];

     wire io_splitter_val = io_splitter_src_val[io_splitter_src_counter];
     wire splitter_io_rdy;

     wire noc1sink_splitter_rdy = splitter_noc1_sink_rdy[splitter_noc1_sink_rdy_counter];
     wire splitter_noc1sink_val;
   
    wire [`NOC_DATA_WIDTH-1:0] noc1_splitter_data = noc1_splitter_src[noc1_splitter_src_counter];
    wire [`NOC_DATA_WIDTH-1:0] noc2_splitter_data = noc2_splitter_src[noc2_splitter_src_counter];
    wire [`NOC_DATA_WIDTH-1:0] splitter_noc3_data;
    wire [`NOC_DATA_WIDTH-1:0] splitter_noc1sink_data;  
    wire [`NOC_DATA_WIDTH-1:0] mem_splitter_data = mem_splitter_src[mem_splitter_src_counter]; 
    wire [`NOC_DATA_WIDTH-1:0] splitter_mem_data;
    wire [`NOC_DATA_WIDTH-1:0] io_splitter_data = io_splitter_src[io_splitter_src_counter];
    wire [`NOC_DATA_WIDTH-1:0] splitter_io_data;

    reg [`NOC_DATA_WIDTH-1:0]   splitter_noc1_sink_ref [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0]   splitter_noc3_mem_sink_ref [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0] 	splitter_noc3_io_sink_ref [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0] 	splitter_mem_sink_ref [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0]   splitter_io_sink_ref [`TEST_DATA_SIZE-1:0];
   
    reg [`NOC_DATA_WIDTH-1:0]   noc1_splitter_src [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0]   noc2_splitter_src [`TEST_DATA_SIZE-1:0];
    reg 			noc1_splitter_src_val [`TEST_DATA_SIZE-1:0];
    reg 		      	noc2_splitter_src_val [`TEST_DATA_SIZE-1:0];

    reg [`NOC_DATA_WIDTH-1:0]   mem_splitter_src [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0]   io_splitter_src [`TEST_DATA_SIZE-1:0];
    reg 		       	mem_splitter_src_val [`TEST_DATA_SIZE-1:0];
    reg 		       	io_splitter_src_val [`TEST_DATA_SIZE-1:0];

    reg [`NOC_DATA_WIDTH-1:0]   splitter_noc1_sink [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0]   splitter_noc3_sink [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0]   splitter_mem_sink [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0]   splitter_io_sink [`TEST_DATA_SIZE-1:0];

    reg 		       	splitter_noc1_sink_rdy [`TEST_DATA_SIZE-1:0];
    reg 		        splitter_mem_sink_rdy [`TEST_DATA_SIZE-1:0];
    reg 			splitter_io_sink_rdy [`TEST_DATA_SIZE-1:0];
    reg 		       	splitter_noc3_sink_rdy [`TEST_DATA_SIZE-1:0];

    reg [`TEST_DATA_LENGTH-1:0] 	noc1_splitter_src_cap;
    reg [`TEST_DATA_LENGTH-1:0]        	noc2_splitter_src_cap;
    reg [`TEST_DATA_LENGTH-1:0] 	mem_splitter_src_cap;
    reg [`TEST_DATA_LENGTH-1:0]        	io_splitter_src_cap;
    reg [`TEST_DATA_LENGTH-1:0]        	memio_splitter_src_cap;
    
    reg [`TEST_DATA_LENGTH-1:0] 	noc1_splitter_src_counter;
    reg [`TEST_DATA_LENGTH-1:0]        	noc2_splitter_src_counter;
    reg [`TEST_DATA_LENGTH-1:0] 	mem_splitter_src_counter;
    reg [`TEST_DATA_LENGTH-1:0]        	io_splitter_src_counter;

    reg [`TEST_DATA_LENGTH-1:0]        	splitter_noc1_sink_counter;
    reg [`TEST_DATA_LENGTH-1:0]        	splitter_noc3_sink_counter;
    reg [`TEST_DATA_LENGTH-1:0] 	splitter_mem_sink_counter;
    reg [`TEST_DATA_LENGTH-1:0]        	splitter_io_sink_counter;
    
    reg [`TEST_DATA_LENGTH-1:0]        	splitter_noc1_sink_rdy_counter;
    reg [`TEST_DATA_LENGTH-1:0]        	splitter_noc3_sink_rdy_counter;
    reg [`TEST_DATA_LENGTH-1:0] 	splitter_mem_sink_rdy_counter;
    reg [`TEST_DATA_LENGTH-1:0]        	splitter_io_sink_rdy_counter;
   
    wire noc1_splitter_src_go = !(noc1_splitter_src_val[noc1_splitter_src_counter] && (!splitter_noc1_rdy));
    wire noc2_splitter_src_go = !(noc2_splitter_src_val[noc2_splitter_src_counter] && (!splitter_noc2_rdy));
    wire mem_splitter_src_go = !(mem_splitter_src_val[mem_splitter_src_counter] && (!splitter_mem_rdy));
    wire io_splitter_src_go = !(io_splitter_src_val[io_splitter_src_counter] && (!splitter_io_rdy));

    wire  splitter_noc1_sink_go = (splitter_noc1_sink_rdy[splitter_noc1_sink_rdy_counter] && splitter_noc1sink_val);
    wire  splitter_noc3_sink_go = (splitter_noc3_sink_rdy[splitter_noc3_sink_rdy_counter] && splitter_noc3_val);
    wire  splitter_mem_sink_go = (splitter_mem_sink_rdy[splitter_mem_sink_rdy_counter] && splitter_mem_val); 
    wire  splitter_io_sink_go = (splitter_io_sink_rdy[splitter_io_sink_rdy_counter] && splitter_io_val);  

    wire   noc1_splitter_src_enable = (noc1_splitter_src_counter != noc1_splitter_src_cap);
    wire   noc2_splitter_src_enable = (noc2_splitter_src_counter != noc2_splitter_src_cap);
    wire   mem_splitter_src_enable = (mem_splitter_src_counter != mem_splitter_src_cap);
    wire   io_splitter_src_enable = (io_splitter_src_counter != io_splitter_src_cap);

    // noc1->splitter->noc1 sink
    always@(posedge clk)
    begin
        if (!rst)
	begin
	    if (noc1_splitter_src_enable && noc1_splitter_src_go)
	    begin
		noc1_splitter_src_counter <= noc1_splitter_src_counter + 1;
	    end
	    if (splitter_noc1_sink_go)
	    begin
	        splitter_noc1_sink_counter <= splitter_noc1_sink_counter + 1;
	        splitter_noc1_sink[splitter_noc1_sink_counter] <= splitter_noc1sink_data;
	    end
	        splitter_noc1_sink_rdy_counter <= splitter_noc1_sink_rdy_counter + 1;
	end
	else
	begin
	    noc1_splitter_src_counter <= 0;
	    noc2_splitter_src_counter <= 0;
	    mem_splitter_src_counter <= 0;
	    io_splitter_src_counter <= 0;

	    splitter_noc1_sink_counter <= 0;
	    splitter_noc3_sink_counter <= 0;
	    splitter_mem_sink_counter <= 0;
	    splitter_io_sink_counter <= 0;

	    splitter_noc1_sink_rdy_counter <= 0;
	    splitter_noc3_sink_rdy_counter <= 0;
	    splitter_mem_sink_rdy_counter <= 0;
	    splitter_io_sink_rdy_counter <= 0;
	end    
    end // always@ (posedge clk)

    // noc2->splitter->mem/io
    always@ (posedge clk)
    begin
        if (!rst)
	begin
	    if (noc2_splitter_src_enable && noc2_splitter_src_go)
	    begin
		noc2_splitter_src_counter <= noc2_splitter_src_counter + 1;
	    end
	   
	    if (splitter_mem_sink_go)
	    begin
	        splitter_mem_sink_counter <= splitter_mem_sink_counter + 1;
	        splitter_mem_sink[splitter_mem_sink_counter] <= splitter_mem_data;
	    end
	    splitter_mem_sink_rdy_counter <= splitter_mem_sink_rdy_counter + 1;
	   
	    if (splitter_io_sink_go)
	    begin
	        splitter_io_sink_counter <= splitter_io_sink_counter + 1;
	        splitter_io_sink[splitter_io_sink_counter] <= splitter_io_data;
	    end
	    splitter_io_sink_rdy_counter <= splitter_io_sink_rdy_counter + 1;
	end
    end // always@ (posedge clk)

    // io/mem->splitter->noc3
    always@ (posedge clk)
    begin
        if (!rst)
	begin
	    if (mem_splitter_src_enable && mem_splitter_src_go)
	    begin
		mem_splitter_src_counter <= mem_splitter_src_counter + 1;
	    end

	    if (io_splitter_src_enable && io_splitter_src_go)
	    begin
		io_splitter_src_counter <= io_splitter_src_counter + 1;
	    end
	   
	    if (splitter_noc3_sink_go)
	    begin
	        splitter_noc3_sink_counter <= splitter_noc3_sink_counter + 1;
	        splitter_noc3_sink[splitter_noc3_sink_counter] <= splitter_noc3_data;
	    end
	    splitter_noc3_sink_rdy_counter <= splitter_noc3_sink_rdy_counter + 1;
	   
	end
    end // always@ (posedge clk)
   
   //----------------------------------------- 
  // Non-syhthesizable part for tb
   //
   //----------------------------------------
   //test data initialization
   `include "mem_io_splitter_tb_data.vh"
   //clck gen
   always
    begin
       #1 clk = ~clk;
    end

    integer i, j, k, msg_length, cap;
   //initialize all sink regs
    initial
    begin
       for (j = 0; j < `TEST_DATA_SIZE; j = j + 1)
       begin
	   splitter_noc1_sink[j] = 0;
	   splitter_mem_sink[j] = 0;
	   splitter_io_sink[j] = 0;
	   splitter_noc3_sink[j] = 0;
       end
    end

    initial
    begin
       $display("mem_io_splitter_tb running");
       $dumpfile("mem_io_splitter.vcd");
       $dumpvars(0, mem_io_splitter_tb);
       for (i = 0; i < 20; i = i + 1)
       begin
	   //$dumpvars(0, splitter_mem_sink[i]);
	   //$dumpvars(0, splitter_noc3_sink[i]);
	   //$dumpvars(0, splitter_noc3_io_sink_ref[i]);
	   //$dumpvars(0, splitter_mem_sink_ref[i]);
       end
       clk = 1;
       rst = 1;
       noc1_splitter_src_cap =  `TEST_DATA_SIZE - 1;
       noc2_splitter_src_cap = `TEST_DATA_SIZE - 1;
       memio_splitter_src_cap = `TEST_DATA_SIZE - 1;
       mem_splitter_src_cap = `TEST_DATA_SIZE - 1;
       io_splitter_src_cap = `TEST_DATA_SIZE - 1;
       #4 rst = 0;
       #2000 $display("testing splitter_noc1_sink[] values");
         for (i = 0; i < noc1_splitter_src_cap; i = i + 1)
	 begin
	    if (splitter_noc1_sink[i] != splitter_noc1_sink_ref[i])
	    begin
	        #1 $display("[FAIL]:splitter_noc1_sink[%d] = %h; expected %h", i, splitter_noc1_sink[i], splitter_noc1_sink_ref[i]);
	    end
	 end
         #1 $display("testing splitter_noc1_sink[] values finishes");
       
       $display("tesing splitter_mem_sink[] values");
	 for (i = 0; i < noc2_splitter_src_cap; i = i + 1)
	 begin
	    if (splitter_mem_sink[i] != splitter_mem_sink_ref[i])
	    begin
	        #1 $display("[FAIL]:splitter_mem_sink[%d] = %h; expected %h", i, splitter_mem_sink[i], splitter_mem_sink_ref[i]);
	    end
	 end
       #1 $display("testing splitter_mem_sink[] values finishes");

       $display("tesing splitter_io_sink[] values");
	 for (i = 0; i < noc2_splitter_src_cap; i = i + 1)
	 begin
	    if (splitter_io_sink[i] != splitter_io_sink_ref[i])
	    begin
	        #1 $display("[FAIL]:splitter_io_sink[%d] = %h; expected %h", i, splitter_io_sink[i], splitter_io_sink_ref[i]);
	    end
	 end
       #1 $display("testing splitter_io_sink[] values finishes");

       $display("tesing splitter_noc3_sink[] values");
       i = 0;
       j = 0;
       k = 0;
       msg_length = 0;
       while (i < memio_splitter_src_cap)
       begin
	    #1
	    if (splitter_noc3_sink[i] == splitter_noc3_io_sink_ref[j])
	    begin
	       msg_length =  {24'b0, splitter_noc3_sink[i][`MSG_LENGTH]} ;
	       cap = i + msg_length;
	       while (i < cap + 1)
	       begin
		   if (splitter_noc3_sink[i] != splitter_noc3_io_sink_ref[j])
		   begin
		       $display("[FAIL]:splitter_noc3_sink[%d] = %h; expected %h (io)", i, splitter_noc3_sink[i], splitter_noc3_io_sink_ref[j]);
		   end
		   i = i + 1;
		   j = j + 1;
	       end
	    end
	    else if (splitter_noc3_sink[i] == splitter_noc3_mem_sink_ref[k])
	    begin
	       msg_length =  {24'b0, splitter_noc3_sink[i][`MSG_LENGTH]};
	       cap = i + msg_length;
	       while (i < cap + 1)
	       begin
		   if (splitter_noc3_sink[i] != splitter_noc3_mem_sink_ref[k])
		   begin
		       $display("[FAIL]:splitter_noc3_sink[%d] = %h; expected %h (mem)", i, splitter_noc3_sink[i], splitter_noc3_mem_sink_ref[k]);
		   end
		   i = i + 1;
		   k = k + 1;
	       end
	    end
	    else
	    begin
	         $display("[FAIL]:splitter_noc3_sink[%d] = %h; expected %h(io) or %h(mem)", 
			    i, splitter_noc3_sink[i], splitter_noc3_io_sink_ref[j], splitter_noc3_mem_sink_ref[k]);
	        i = i + 1;
	    end // else: !if(splitter_noc3_sink[i] == splitter_noc3_mem_sink_ref[k])   
       end // while (i < memio_splitter_src_cap)


       while (k < `TEST_DATA_SIZE)
       begin
           if (splitter_noc3_mem_sink_ref[k] != 0)
           begin
       	    $display("[FAIL]:splitter_noc3_mem_sink_ref[%d] = %h not loaded", k, splitter_noc3_mem_sink_ref[k]);
       	end
           k = k + 1;
       end
       while (j < `TEST_DATA_SIZE)
       begin
           if (splitter_noc3_io_sink_ref[k] != 0)
           begin
       	    $display("[FAIL]:splitter_noc3_io_sink_ref[%d] = %h not loaded", j, splitter_noc3_io_sink_ref[j]);
       	end
           j = j + 1;
       end	 
       $display("testing splitter_noc3_sink[] values finishes");
       #10000 $finish;
    end

    

endmodule