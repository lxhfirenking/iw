//------------------------------
// testbench for iosplitter_axi_lite
// not synthesizable
//------------------------------
`include "define.vh"
`timescale 1ns/1ns
`define TEST_DATA_SIZE 2048
`define TEST_DATA_LENGTH 11
`define C_M_AXI_LITE_DATA_WIDTH  `NOC_DATA_WIDTH
`define C_M_AXI_LITE_ADDR_WIDTH  `NOC_DATA_WIDTH
`define C_M_AXI_LITE_RESP_WIDTH  2

module iosplitter_axi_lite_tb();
    
    iosplitter_axi_lite iosplitter_axi_lite_dut(
    .clk (clk),
    .rst (rst),
			   
    //signal from the splitter
    // rdy/val for splitter to IO
    .splitter_io_val (splitter_io_val),
    .io_splitter_rdy (io_splitter_rdy),

    // rdy/val for IO to splitter
    .io_splitter_val (io_splitter_val),
    .splitter_io_rdy (splitter_io_rdy),

    //data to and from splitter
    .io_splitter_data (io_splitter_data),
    .splitter_io_data (splitter_io_data),
			   
    //axi lite signals			   
    //write address channel
    .m_axi_awaddr   (m_axi_awaddr), 
    .m_axi_awvalid  (m_axi_awvalid),
    .m_axi_awready  (m_axi_awready),

    //write data channel
    .m_axi_wdata   (m_axi_wdata),
    .m_axi_wstrb   (m_axi_wstrb),
    .m_axi_wvalid  (m_axi_wvalid),
    .m_axi_wready  (m_axi_wready),

    //read address channel
    .m_axi_araddr  (m_axi_araddr), 
    .m_axi_arvalid (m_axi_arvalid),
    .m_axi_arready (m_axi_arready),

    //read data channel
    .m_axi_rdata  (m_axi_rdata),
    .m_axi_rresp  (m_axi_rresp),
    .m_axi_rvalid (m_axi_rvalid),
    .m_axi_rready (m_axi_rready),

    //write response channel
    .m_axi_bresp  (m_axi_bresp),
    .m_axi_bvalid (m_axi_bvalid),
    .m_axi_bready (m_axi_bready)

    // two addr protect constant port not included yet...
    );

    reg clk;
    reg rst;
   
    wire splitter_io_val = splitter_io_src_val[splitter_io_src_counter];
    wire io_splitter_rdy;

    wire io_splitter_val;
    wire splitter_io_rdy = io_splitter_sink_rdy[io_splitter_sink_rdy_counter];

    wire [`NOC_DATA_WIDTH-1:0] io_splitter_data;
    wire [`NOC_DATA_WIDTH-1:0] splitter_io_data = splitter_io_src[splitter_io_src_counter];

    //axi lite signals			   
    //write address channel
    wire [`C_M_AXI_LITE_ADDR_WIDTH-1:0] m_axi_awaddr;
    wire 			       m_axi_awvalid;
    wire 			       m_axi_awready = m_axi_aw_sink_rdy[m_axi_aw_sink_rdy_counter];

    //write data channel
    wire [`C_M_AXI_LITE_DATA_WIDTH-1:0] m_axi_wdata;
    wire [`C_M_AXI_LITE_DATA_WIDTH/8-1:0] m_axi_wstrb;
    wire 				 m_axi_wvalid;
    wire 				 m_axi_wready = m_axi_w_sink_rdy[m_axi_w_sink_rdy_counter];

    //read address channel
    wire [`C_M_AXI_LITE_ADDR_WIDTH-1:0] 	 m_axi_araddr;
    wire 				 m_axi_arvalid;
    wire 				 m_axi_arready = m_axi_ar_sink_rdy[m_axi_ar_sink_rdy_counter];

    //read data channel
    wire [`C_M_AXI_LITE_DATA_WIDTH-1:0] 	 m_axi_rdata = m_axi_r_src[m_axi_r_src_counter];
    wire [`C_M_AXI_LITE_RESP_WIDTH-1:0] 	 m_axi_rresp = m_axi_r_src_resp[m_axi_r_src_counter];
    wire 				 m_axi_rvalid = m_axi_r_src_val[m_axi_r_src_counter];
    wire 				 m_axi_rready;

    //write response channel
    wire [`C_M_AXI_LITE_RESP_WIDTH-1:0] 	 m_axi_bresp = m_axi_b_src_resp[m_axi_b_src_counter];
    wire 				 m_axi_bvalid = m_axi_b_src_val[m_axi_b_src_counter];
    wire 				 m_axi_bready;


    reg [`NOC_DATA_WIDTH-1:0] splitter_io_src [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_DATA_WIDTH-1:0] m_axi_r_src [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_RESP_WIDTH-1:0] m_axi_r_src_resp [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_RESP_WIDTH-1:0] m_axi_b_src_resp [`TEST_DATA_SIZE-1:0];

    reg 				      splitter_io_src_val [`TEST_DATA_SIZE-1:0];
    reg 				      m_axi_r_src_val [`TEST_DATA_SIZE-1:0];
    reg 				      m_axi_b_src_val [`TEST_DATA_SIZE-1:0];
   
    reg [`NOC_DATA_WIDTH-1:0] 		io_splitter_sink [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_ADDR_WIDTH-1:0] 	m_axi_aw_sink [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_DATA_WIDTH-1:0] 	m_axi_w_sink [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_DATA_WIDTH/8-1:0] m_axi_w_sink_strb [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_ADDR_WIDTH-1:0] 	m_axi_ar_sink [`TEST_DATA_SIZE-1:0];

    reg 					io_splitter_sink_rdy [`TEST_DATA_SIZE-1:0];
    reg 					m_axi_aw_sink_rdy [`TEST_DATA_SIZE-1:0];
    reg 					m_axi_w_sink_rdy [`TEST_DATA_SIZE-1:0];
    reg 					m_axi_ar_sink_rdy [`TEST_DATA_SIZE-1:0];
   
    reg [`NOC_DATA_WIDTH-1:0] 		io_splitter_l_ack_sink_ref [`TEST_DATA_SIZE-1:0];
    reg [`NOC_DATA_WIDTH-1:0]       io_splitter_s_ack_sink_ref [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_ADDR_WIDTH-1:0] 	m_axi_aw_sink_ref [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_DATA_WIDTH-1:0] 	m_axi_w_sink_ref [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_DATA_WIDTH/8-1:0] m_axi_w_sink_strb_ref [`TEST_DATA_SIZE-1:0];
    reg [`C_M_AXI_LITE_ADDR_WIDTH-1:0] 	m_axi_ar_sink_ref [`TEST_DATA_SIZE-1:0];

    reg [`TEST_DATA_LENGTH-1:0] 		splitter_io_src_counter;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_r_src_counter;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_b_src_counter;

    reg [`TEST_DATA_LENGTH-1:0] 		splitter_io_src_cap;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_r_src_cap;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_b_src_cap;

    reg [`TEST_DATA_LENGTH-1:0] 		io_splitter_sink_counter;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_aw_sink_counter;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_w_sink_counter;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_ar_sink_counter;

    reg [`TEST_DATA_LENGTH-1:0] 		io_splitter_sink_rdy_counter;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_aw_sink_rdy_counter;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_w_sink_rdy_counter;
    reg [`TEST_DATA_LENGTH-1:0] 		m_axi_ar_sink_rdy_counter;

    wire         splitter_io_src_go = !(splitter_io_src_val[splitter_io_src_counter] && (!io_splitter_rdy));
    wire         m_axi_r_src_go = !(m_axi_r_src_val[m_axi_r_src_counter] && (!m_axi_rready));
    wire         m_axi_b_src_go = !(m_axi_b_src_val[m_axi_b_src_counter] && (!m_axi_bready));

    wire         io_splitter_sink_go = (io_splitter_sink_rdy[io_splitter_sink_rdy_counter] && io_splitter_val);
    wire         m_axi_aw_sink_go = (m_axi_aw_sink_rdy[m_axi_aw_sink_rdy_counter] && m_axi_awvalid);
    wire         m_axi_w_sink_go = (m_axi_w_sink_rdy[m_axi_w_sink_rdy_counter] && m_axi_wvalid);
    wire         m_axi_ar_sink_go = (m_axi_ar_sink_rdy[m_axi_ar_sink_rdy_counter] && m_axi_arvalid);

    wire         splitter_io_src_enable = (splitter_io_src_counter != splitter_io_src_cap);
    wire         m_axi_r_src_enable = (m_axi_r_src_counter != m_axi_r_src_cap);
    wire         m_axi_b_src_enable = (m_axi_b_src_counter != m_axi_b_src_cap);

    
    always @ (posedge clk)
    begin
        if (!rst)
        begin
            //splitter_io_src -> iosplitter_axi_lite
            if (splitter_io_src_go && splitter_io_src_enable)
            begin
                splitter_io_src_counter <= splitter_io_src_counter + 1;
            end

            //m_axi_r_src -> iosplitter_axi_lite
            if (m_axi_r_src_go && m_axi_r_src_enable)
            begin
                m_axi_r_src_counter <= m_axi_r_src_counter + 1;
            end

            //m_axi_w_src -> iosplitter_axi_lite
            if (m_axi_b_src_go && m_axi_b_src_enable)
            begin
                m_axi_b_src_counter <= m_axi_b_src_counter + 1;
            end

            //iosplitter_axi_lite -> m_axi_ar_sink
            if (m_axi_ar_sink_go)
            begin
                m_axi_ar_sink_counter <= m_axi_ar_sink_counter + 1;
                m_axi_ar_sink[m_axi_ar_sink_counter] <= m_axi_araddr;
            end
            m_axi_ar_sink_rdy_counter <= m_axi_ar_sink_rdy_counter + 1;

            //iosplitter_axi_lite -> m_axi_aw_sink
            if (m_axi_aw_sink_go)
            begin
                m_axi_aw_sink_counter <= m_axi_aw_sink_counter + 1;
                m_axi_aw_sink[m_axi_aw_sink_counter] <= m_axi_awaddr;
            end
            m_axi_aw_sink_rdy_counter <= m_axi_aw_sink_rdy_counter + 1;

            //iosplitter_axi_lite -> m_axi_w_sink
            if (m_axi_w_sink_go)
            begin
                m_axi_w_sink_counter <= m_axi_w_sink_counter + 1;
                m_axi_w_sink[m_axi_w_sink_counter] <= m_axi_wdata;
                m_axi_w_sink_strb[m_axi_w_sink_counter] <= m_axi_wstrb;
            end
            m_axi_w_sink_rdy_counter <= m_axi_w_sink_rdy_counter + 1;

            // iosplitter_axi_lite -> io_splitter_sink
            if (io_splitter_sink_go)
            begin
                io_splitter_sink_counter <= io_splitter_sink_counter + 1;
                io_splitter_sink[io_splitter_sink_counter] <= io_splitter_data;
            end
            io_splitter_sink_rdy_counter <= io_splitter_sink_rdy_counter + 1;
        end
        else
        begin
            splitter_io_src_counter <= 0;
            m_axi_b_src_counter <= 0;
            m_axi_r_src_counter <= 0;

            m_axi_ar_sink_counter <= 0;
            m_axi_aw_sink_counter <= 0;
            m_axi_w_sink_counter <= 0;
            io_splitter_sink_counter <= 0;

            m_axi_ar_sink_rdy_counter <= 0;
            m_axi_aw_sink_rdy_counter <= 0;
            m_axi_w_sink_rdy_counter <= 0;
            io_splitter_sink_rdy_counter <= 0;
        end
    end

    //----------------------------------------- 
    // Non-syhthesizable part for tb
    //
    //----------------------------------------
    //test data initialization
    `include "iosplitter_axi_lite_tb_data.vh"
    //initialize all sink regs
    integer i, j, k, msg_length, cap;
    initial
    begin
        for (i = 0; i < `TEST_DATA_SIZE; i = i + 1)
        begin
            io_splitter_sink[i] = 0;
            m_axi_aw_sink[i] = 0;
            m_axi_w_sink[i] = 0;
            m_axi_w_sink_strb[i] = 0;
            m_axi_ar_sink[i] = 0;
        end
    end

    //start the test and check the sinks
    integer counter;
    initial
    begin
        $display("iosplitter_axi_lite_tb running");
        $dumpfile("iosplitter_axi_lite.vcd");
        $dumpvars(0, iosplitter_axi_lite_tb);

        //set up initial parameters
        clk = 1;
        rst = 1;
        splitter_io_src_cap = `TEST_DATA_SIZE - 1;
        m_axi_r_src_cap = `TEST_DATA_SIZE - 1;
        m_axi_b_src_cap = `TEST_DATA_SIZE - 1;

        #4 rst = 0; 

        // testing m_axi_ar_sink[]
        #10000 counter = 0;
        $display("testing m_axi_ar_sink[] values");
        for (i = 0; i < splitter_io_src_cap; i = i + 1)
        begin
            if (m_axi_ar_sink[i] == m_axi_ar_sink_ref[i])
            begin
                counter = counter + 1;
            end
            else
            begin
                $display("[FAIL]:m_axi_ar_sink[%d] = %h; expected %h",
                  i, m_axi_ar_sink[i], m_axi_ar_sink_ref[i]);  
            end
        end

        if (counter == splitter_io_src_cap)
        begin
            $display("testing m_axi_ar_sink[] values successful");
        end
        else
        begin
            $display("testing m_axi_ar_sink[] values FAILED");
        end

        // testing m_axi_aw_sink[]
        counter = 0;
        $display("testing m_axi_aw_sink[] values");
        for (i = 0; i < splitter_io_src_cap; i = i + 1)
        begin
            if (m_axi_aw_sink[i] == m_axi_aw_sink_ref[i])
            begin
                counter = counter + 1;
            end
            else
            begin
                $display("[FAIL]:m_axi_aw_sink[%d] = %h; expected %h",
                  i, m_axi_aw_sink[i], m_axi_aw_sink_ref[i]);  
            end
        end

        if (counter == splitter_io_src_cap)
        begin
            $display("testing m_axi_aw_sink[] values successful");
        end
        else
        begin
            $display("testing m_axi_aw_sink[] values FAILED");
        end

        // testing m_axi_w_sink[]
        counter = 0;
        $display("testing m_axi_w_sink[] values");
        for (i = 0; i < splitter_io_src_cap; i = i + 1)
        begin
            if (m_axi_w_sink[i] == m_axi_w_sink_ref[i])
            begin
                counter = counter + 1;
            end
            else
            begin
                $display("[FAIL]:m_axi_w_sink[%d] = %h; expected %h",
                  i, m_axi_w_sink[i], m_axi_w_sink_ref[i]);  
            end
        end

        if (counter == splitter_io_src_cap)
        begin
            $display("testing m_axi_w_sink[] values successful");
        end
        else
        begin
            $display("testing m_axi_w_sink[] values FAILED");
        end

        // testing m_axi_w_sink_strb[]
        counter = 0;
        $display("testing m_axi_w_sink_strb[] values");
        for (i = 0; i < splitter_io_src_cap; i = i + 1)
        begin
            if (m_axi_w_sink_strb [i]  == m_axi_w_sink_strb_ref[i])
            begin
                counter = counter + 1;
            end
            else
            begin
                $display("[FAIL]:m_axi_w_sink_strb[%d] = %h; expected %h",
                  i, m_axi_w_sink_strb[i], m_axi_w_sink_strb_ref[i]);  
            end
        end

        if (counter == splitter_io_src_cap)
        begin
            $display("testing m_axi_w_sink[] values successful");
        end
        else
        begin
            $display("testing m_axi_w_sink[] values FAILED");
        end

        // testing io_splitter_sink[]..
        $display("testing io_splitter_sink[] values");
        i = 0;
        j = 0;
        k = 0;
        counter = 0;
        cap = 0;
        msg_length = 0;
        while (i < splitter_io_src_cap)
        begin
            if (io_splitter_sink[i] == io_splitter_l_ack_sink_ref[j])
            begin
                msg_length =  {24'b0, io_splitter_sink[i][`MSG_LENGTH]} ;
                cap = i + msg_length;
                while (i < cap + 1)
                begin
                    if (io_splitter_sink[i] != io_splitter_l_ack_sink_ref[j])
                    begin
                        $display("[FAIL]:io_splitter_sink[%d] = %h; expected %h (load_ack)", i, io_splitter_sink[i], io_splitter_l_ack_sink_ref[j]);
                    end
                    i = i + 1;
                    j = j + 1;
                end
            end
            else if (io_splitter_sink[i] == io_splitter_s_ack_sink_ref[k])
            begin
                msg_length =  {24'b0, io_splitter_sink[i][`MSG_LENGTH]};
                cap = i + msg_length;
                while (i < cap + 1)
                begin
                    if (io_splitter_sink[i] != io_splitter_s_ack_sink_ref[k])
                    begin
                        $display("[FAIL]:io_splitter_sink[%d] = %h; expected %h (store_ack)", i, io_splitter_sink[i], io_splitter_s_ack_sink_ref[k]);
                    end
                    i = i + 1;
                    k = k + 1;
                end
            end
            else
            begin
                $display("[FAIL]:io_splitter_sink[%d] = %h; expected %h(load_ack) or %h(store_ack)", 
                i, io_splitter_sink[i], io_splitter_l_ack_sink_ref[j], io_splitter_s_ack_sink_ref[k]);
                i = i + 1;
            end
        end


        while (k < `TEST_DATA_SIZE)
        begin
            if (io_splitter_s_ack_sink_ref[k] != 0)
            begin
                $display("[FAIL]:io_splitter_s_ack_sink_ref[%d] = %h not received", k, io_splitter_s_ack_sink_ref[k]);
            end
           k = k + 1;
        end

        while (j < `TEST_DATA_SIZE)
        begin
            if (io_splitter_l_ack_sink_ref[k] != 0)
            begin
                $display("[FAIL]:io_splitter_l_ack_sink_ref[%d] = %h not received", j, io_splitter_l_ack_sink_ref[j]);
            end
            j = j + 1;
        end   
        $display("testing io_splitter_sink[] values successful");
        

        //still need to check write data...
        #20000 $finish;
    end
    //clck gen
    always
    begin
        #1 clk = ~clk;
    end
endmodule