#!/usr/bin/python
import os
from random import randint

def gen_rand_msg(src, sinkref, val):
    msg_seed0 = randint(0, 0xffffffff)
    msg_seed1 = randint(0, 0xffffffff)
    data = "{0:032b}".format(msg_seed0) + "{0:032b}".format(msg_seed1)
    # convert to hex
    data = "{0:016x}".format(int(data, 2))
    src.append(data)
    sinkref.append(data)
    val.append("1")

# insert a random delay 
def gen_rand_delay_src(src, val, rand_delay_factor):
    seed = randint(0, 99)
    while (seed < rand_delay_factor) :
        src.append("0")
        val.append("0")
        seed = randint(0, 99)

#TODO: force load pl_length to be 2, io store pl_length to be 3
#by default sinkref1 is mem and sinkref2 is io    
def gen_memio_request(src, sinkrefmem, sinkrefio, val, pl_length, toIO):
    #generate word 1
    chipid = "{0:014b}".format(1)
    xpos = "{0:08b}".format(15)
    ypos = "{0:08b}".format(15)
    fbits = "{0:04b}".format(0)
    payload_length = "{0:08b}".format(pl_length)
    msg_type = "{0:08b}".format(0)
    mshr_tag = "{0:08b}".format(0)
    options1 = "{0:06b}".format(1)
    word1 = chipid + xpos + ypos + fbits + payload_length + msg_type + mshr_tag + options1
    # convert to hex
    word1 = "{0:016x}".format(int(word1, 2))
    gen_rand_delay_src(src, val, RAND_DELAY_FACTOR)
    val.append("1")
    src.append(word1)
    if (not(toIO)):
        sinkrefmem.append(word1)
    else:
        sinkrefio.append(word1)
    
    #generate word 2
    add_seed0 = randint(0, 1048575)
    add_seed1 = randint(0, 1048575)
    # Note: limit top bits to 0x9a for now, should switch back to top bit 1
    mask = 524288 #2^19
    if toIO:
        #add_seed1 = add_seed1 | mask
        add_seed1 = randint(0x9a000, 0x9afff)
    else:
        add_seed1 = add_seed1 & (mask -1)
    options2 = "{0:016b}".format(0);
    padding = "{0:08}".format(0);
    word2 = padding + "{0:020b}".format(add_seed1) + "{0:020b}".format(add_seed0) + options2
    # convert to hex
    word2 = "{0:016x}".format(int(word2, 2))
    gen_rand_delay_src(src, val, RAND_DELAY_FACTOR)
    val.append("1")
    src.append(word2)
    if (not(toIO)):
        sinkrefmem.append(word2)
    else:
        sinkrefio.append(word2)

    #generate word 3
    src_chipid = "{0:014b}".format(3)
    src_x = "{0:08b}".format(7)
    src_y = "{0:08b}".format(7)
    src_fbit = "{0:04b}".format(0)
    reserved = "{0:030b}".format(0)
    word3 = src_chipid + src_x + src_y + src_fbit + reserved
    # convert to hex
    word3 = "{0:016x}".format(int(word3, 2))
    gen_rand_delay_src(src, val, RAND_DELAY_FACTOR)
    val.append("1")
    src.append(word3)
    if (not(toIO)):
        sinkrefmem.append(word3)
    else:
        sinkrefio.append(word3)

    #generate word
    for i in range(2, pl_length):
        gen_rand_delay_src(src, val, RAND_DELAY_FACTOR)
        if (not(toIO)):
            msg = gen_rand_msg(src, sinkrefmem, val)
        else:
            msg = gen_rand_msg(src, sinkrefio, val)

def gen_memio_response(src, sinkref, val, pl_length, fromIO):
    #generate word 1
    chipid = "{0:014b}".format(1)
    xpos = "{0:08b}".format(15)
    ypos = "{0:08b}".format(15)
    fbits = "{0:04b}".format(0)
    payload_length = "{0:08b}".format(pl_length)
    msg_type = "{0:08b}".format(0)
    mshr_tag = "{0:08b}".format(0)
    # temperarily use reserve bit to make rsp from IO to be diff from rsp from mem
    if (fromIO):
        reserved = "{0:06b}".format(1)
    else:
        reserved = "{0:06b}".format(0)
    word1 = chipid + xpos + ypos + fbits + payload_length + msg_type + mshr_tag + reserved
    gen_rand_delay_src(src, val, RAND_DELAY_FACTOR * 2)
    # convert to hex
    word1 = "{0:016x}".format(int(word1, 2))
    val.append("1")
    src.append(word1)
    sinkref.append(word1)
    
    #generate word
    for i in range(0, pl_length):
        gen_rand_delay_src(src, val, RAND_DELAY_FACTOR)
        msg = gen_rand_msg(src, sinkref, val)

def filename(s):
    return (os.environ["MEM_IO_SPLITTER_ROOT"] + s)

def test1(f):
    f.write("this is a test%d\n" %(0))

def test2(f):
    f.write("this is the second test%x\n" %(12))

def gen_random_ready(length):
    tmp = []
    for i in range(length):
        seed = randint(0, 99)
        if (seed < 33):
            tmp.append("0")
        else:
            tmp.append("1")
    return tmp

        

def gen_data_file(array, unit_name, bit_length):
    s = '/tb_data/%s.data.v' %(unit_name)
    f = open (filename(s), 'w');
    f.write("initial\nbegin\n")
    for i in range(len(array)):
        f.write("\t%s[%d] = %d'h%s;\n" %(unit_name, i, bit_length, array[i]))
    for i in range(len(array), TEST_LENGTH):
        f.write("\t%s[%d] = %d'h%d;\n" %(unit_name, i, bit_length, 0));
    f.write("end")
    f.close() 

def gen_noc1_splitter_src():
    noc1_splitter_src_val = []
    noc1_splitter_src = []
    splitter_noc1_sink_ref = []
    splitter_noc1_sink_rdy = gen_random_ready(TEST_LENGTH)
    for i in range(NOC1_SRC_MSG_COUNT):
        length = randint(2, 10)
        seed = randint(0,1)
        gen_memio_request(noc1_splitter_src, splitter_noc1_sink_ref, splitter_noc1_sink_ref, noc1_splitter_src_val, length, (seed == 1))
    gen_data_file(noc1_splitter_src, "noc1_splitter_src", 64)
    gen_data_file(noc1_splitter_src_val, "noc1_splitter_src_val", 1)
    gen_data_file(splitter_noc1_sink_ref, "splitter_noc1_sink_ref", 64)
    gen_data_file(splitter_noc1_sink_rdy, "splitter_noc1_sink_rdy", 1)

def gen_noc2_splitter_src():
    noc2_splitter_src_val = []
    noc2_splitter_src = []
    splitter_mem_sink_ref = []
    splitter_io_sink_ref = []
    splitter_mem_sink_rdy = gen_random_ready(TEST_LENGTH)
    splitter_io_sink_rdy = gen_random_ready(TEST_LENGTH)
    for i in range(NOC2_SRC_MSG_COUNT):
        length = randint(2, 10)
        seed = randint(0,1)
        gen_memio_request(noc2_splitter_src, splitter_mem_sink_ref, splitter_io_sink_ref, noc2_splitter_src_val, length, (seed == 1))
    gen_data_file(noc2_splitter_src, "noc2_splitter_src", 64)
    gen_data_file(noc2_splitter_src_val, "noc2_splitter_src_val", 1)
    gen_data_file(splitter_mem_sink_ref, "splitter_mem_sink_ref", 64)
    gen_data_file(splitter_mem_sink_rdy, "splitter_mem_sink_rdy", 1)
    gen_data_file(splitter_io_sink_ref, "splitter_io_sink_ref", 64)
    gen_data_file(splitter_io_sink_rdy, "splitter_io_sink_rdy", 1)
            
def gen_memio_splitter_src():
    mem_splitter_src = []
    mem_splitter_src_val = []
    io_splitter_src = []
    io_splitter_src_val = []
    splitter_noc3_mem_sink_ref = []
    splitter_noc3_io_sink_ref = []
    splitter_noc3_sink_rdy = gen_random_ready(TEST_LENGTH)
    for i in range(IO_SRC_MSG_COUNT):
        length = randint(0, 8)
        gen_memio_response(io_splitter_src, splitter_noc3_io_sink_ref, io_splitter_src_val, length, True)
    gen_data_file(io_splitter_src, "io_splitter_src", 64)
    gen_data_file(io_splitter_src_val, "io_splitter_src_val", 1)
    gen_data_file(splitter_noc3_io_sink_ref, "splitter_noc3_io_sink_ref", 64)

    for i in range(MEM_SRC_MSG_COUNT):
        length = randint(0, 8)
        gen_memio_response(mem_splitter_src, splitter_noc3_mem_sink_ref, mem_splitter_src_val, length, False)
    gen_data_file(mem_splitter_src, "mem_splitter_src", 64)
    gen_data_file(mem_splitter_src_val, "mem_splitter_src_val", 1)
    gen_data_file(splitter_noc3_mem_sink_ref, "splitter_noc3_mem_sink_ref", 64)

    gen_data_file(splitter_noc3_sink_rdy, "splitter_noc3_sink_rdy", 1)
    
#--------------------------------------------------------
TEST_LENGTH = 1024
RAND_DELAY_FACTOR = 30
NOC1_SRC_MSG_COUNT = 40
NOC2_SRC_MSG_COUNT = 40
IO_SRC_MSG_COUNT = 40
MEM_SRC_MSG_COUNT = 40
gen_noc1_splitter_src()
gen_noc2_splitter_src()
gen_memio_splitter_src()
