#!/usr/bin/python
import os
from random import randint

def gen_rand_msg(src, sinkref, val):
    msg_seed0 = randint(0, 0xffffffff)
    msg_seed1 = randint(0, 0xffffffff)
    data = "{0:032b}".format(msg_seed0) + "{0:032b}".format(msg_seed1)
    # convert to hex
    data = "{0:016x}".format(int(data, 2))
    src.append(data)
    sinkref.append(data)
    val.append("1")

# insert a random delay 
def gen_rand_delay_src(src, val, rand_delay_factor, src_resp = []):
    seed = randint(0, 99)
    while (seed < rand_delay_factor) :
        src.append("0")
        val.append("0")
        src_resp.append("0")
        seed = randint(0, 99)

# if the input array is shorter than length, then append it with delay/inval until the length match up
def add_delay_to(src_resp, val, length, src = []):
    while (len(src_resp) < length):
        src.append("0")
        src_resp.append("0")
        val.append("0")

#generate a ready array of length 
def gen_random_ready(length):
    tmp = []
    for i in range(length):
        seed = randint(0, 99)
        if (seed < RAND_READY_FACTOR):
            tmp.append("0")
        else:
            tmp.append("1")
    return tmp

#generate a data file from an array
def gen_data_file(array, unit_name, bit_length):
    s = '/%s.data.v' %(unit_name)
    f = open (filename(s), 'w');
    f.write("initial\nbegin\n")
    for i in range(len(array)):
        f.write("\t%s[%d] = %d'h%s;\n" %(unit_name, i, bit_length, array[i]))
    for i in range(len(array), TEST_LENGTH):
        f.write("\t%s[%d] = %d'h%d;\n" %(unit_name, i, bit_length, 0));
    f.write("end")
    f.close() 

#fullpath file name
def filename(s):
    return (os.environ["IOSPLITTER_AXI_LITE_ROOT"] + "/iosplitter_axi_lite_tb_data/" + s)

#generate io request
def gen_io_request(splitter_io_src, r_src, r_src_resp, b_src_resp, 
                   splitter_io_src_val, r_src_val, b_src_val, 
                   io_splitter_l_ack_sink, io_splitter_s_ack_sink,
                   aw_sink, w_sink, w_sink_strb, ar_sink,
                   is_load):
    #generate word 1
    pl_length = 3
    msg_type_content = MSG_TYPE_STORE_REQ
    if (is_load):
        pl_length = 2
        msg_type_content = MSG_TYPE_LOAD_REQ
    chipid = "{0:014b}".format(1)
    xpos = "{0:08b}".format(15)
    ypos = "{0:08b}".format(15)
    fbits = "{0:04b}".format(0)
    payload_length = "{0:08b}".format(pl_length)
    msg_type = "{0:08b}".format(msg_type_content)
    mshr_tag = randint(0, 0xff)
    mshr_tag = "{0:08b}".format(mshr_tag)
    reserved = "{0:06b}".format(1)
    word1 = chipid + xpos + ypos + fbits + payload_length + msg_type + mshr_tag + reserved
    gen_rand_delay_src(splitter_io_src, splitter_io_src_val, RAND_DELAY_FACTOR)
    # convert to hex
    word1 = "{0:016x}".format(int(word1, 2))
    splitter_io_src_val.append("1")
    splitter_io_src.append(word1)

    #generate word 2
    add_seed0 = randint(0, 1048575)
    # Note: limit top bits to 0x9a for now, should switch back to top bit 1
    #add_seed1 = randint(0x9a000, 0x9afff)
    add_seed1 = randint(0, 1048575)
    # ensure that addr to io has msb of 1
    mask = 524288 #2^19
    add_seed1 = add_seed1 | mask

    #generate random data_size
    data_size = randint(0, 4);
    bstrb_sink_value = 0xff;
    if (data_size == MSG_DATA_SIZE_0B):
        bstrb_sink_value = 0x0;
    elif (data_size == MSG_DATA_SIZE_1B):
        bstrb_sink_value = 0x1;
    elif (data_size == MSG_DATA_SIZE_2B):
        bstrb_sink_value = 0x3;
    elif (data_size == MSG_DATA_SIZE_4B):
        bstrb_sink_value = 0xf;
    elif (data_size == MSG_DATA_SIZE_8B):
        bstrb_sink_value = 0xff;
    bstrb_sink_value = "{0:02x}".format(bstrb_sink_value)

    #we only care about datasize field
    options2 = "{0:04b}".format(0) + "{0:01b}".format(0) + "{0:03b}".format(data_size) + "{0:08b}".format(0);
    padding = "{0:08}".format(0);
    word2 = padding + "{0:020b}".format(add_seed1) + "{0:020b}".format(add_seed0) + options2
    gen_rand_delay_src(splitter_io_src, splitter_io_src_val, RAND_DELAY_FACTOR)
    # convert to hex
    word2 = "{0:016x}".format(int(word2, 2))
    splitter_io_src_val.append("1")
    splitter_io_src.append(word2)
    address = "{0:024}".format(0) + "{0:020b}".format(add_seed1) + "{0:020b}".format(add_seed0)
    address = "{0:016x}".format(int(address, 2))
    if (is_load):
        ar_sink.append(address)
    else:
        aw_sink.append(address)

    #generate word 3
    src_chipid = randint(0, 0x3fff)
    src_chipid = "{0:014b}".format(src_chipid)
    src_x = randint(0, 0xff)
    src_x = "{0:08b}".format(src_x)
    src_y = randint(0, 0xff)
    src_y = "{0:08b}".format(src_y)
    src_fbit = randint(0, 0xf)
    src_fbit = "{0:04b}".format(src_fbit)
    reserved = "{0:030b}".format(0)
    word3 = src_chipid + src_x + src_y + src_fbit + reserved
    # convert to hex
    word3 = "{0:016x}".format(int(word3, 2))
    gen_rand_delay_src(splitter_io_src, splitter_io_src_val, RAND_DELAY_FACTOR)
    splitter_io_src_val.append("1")
    splitter_io_src.append(word3)
    
    #add data word for store
    if (not(is_load)):
        gen_rand_delay_src(splitter_io_src, splitter_io_src_val, RAND_DELAY_FACTOR)
        gen_rand_msg(splitter_io_src, w_sink, splitter_io_src_val)
        w_sink_strb.append(bstrb_sink_value)
        
    
    #generate response values
    if (is_load):
        add_delay_to(r_src_resp, r_src_val, len(splitter_io_src) + 2, src = r_src)
        msg_type =  msg_type = "{0:08b}".format(MSG_TYPE_LOAD_MEM_ACK)
        payload_length = "{0:08b}".format(8)
        reserved = "{0:06b}".format(0)
        load_resp_word1 = (src_chipid + src_x + src_y 
                           + src_fbit + payload_length 
                           + msg_type + mshr_tag + reserved)
        #convert to hex
        load_resp_word1 = "{0:016x}".format(int(load_resp_word1, 2))
        
        #data should repeat on everybyte
        data_seed = randint(0, 0xff)
        #combine and convert to hex
        r_src_data = "{0:016x}".format(data_seed)
        r_src_data_masked = ""
        for i in range(0, 8):
            r_src_data_masked = r_src_data_masked + "{0:02x}".format(data_seed)
        r_src_resp_value = "{0:01x}".format(AXI_LITE_RRESP_VALUE); # response always valid
        gen_rand_delay_src(r_src, r_src_val, RAND_DELAY_FACTOR, src_resp = r_src_resp)
        r_src.append(r_src_data)
        r_src_resp.append(r_src_resp_value)
        r_src_val.append("1")

        #load the potential response into sinks
        io_splitter_l_ack_sink.append(load_resp_word1)
        for i in range(0, 8):
            io_splitter_l_ack_sink.append(r_src_data_masked)
        
    else:
        add_delay_to(b_src_resp, b_src_val, len(splitter_io_src) + 2)
        msg_type =  msg_type = "{0:08b}".format(MSG_TYPE_NC_STORE_MEM_ACK)
        payload_length = "{0:08b}".format(0)
        reserved = "{0:06b}".format(0)
        store_resp_word1 = (src_chipid + src_x + src_y 
                           + src_fbit + payload_length 
                           + msg_type + mshr_tag + reserved)
        #convert to hex
        store_resp_word1 = "{0:016x}".format(int(store_resp_word1, 2))
        
        b_src_resp_value = "{0:01x}".format(AXI_LITE_BRESP_VALUE); # response always valid
        gen_rand_delay_src(b_src_resp, b_src_val, RAND_DELAY_FACTOR)
        b_src_resp.append(b_src_resp_value)
        b_src_val.append("1")

        #load the potential response into sink
        io_splitter_s_ack_sink.append(store_resp_word1)
        
def gen_spliter_io_axi_src():
    splitter_io_src = []
    m_axi_r_src = []
    m_axi_r_src_resp = []
    m_axi_b_src_resp = []
    
    splitter_io_src_val = []
    m_axi_r_src_val = []
    m_axi_b_src_val = []
    
    io_splitter_l_ack_sink_ref = []
    io_splitter_s_ack_sink_ref = []
    m_axi_aw_sink_ref = []
    m_axi_w_sink_ref = []
    m_axi_w_sink_strb_ref = []
    m_axi_ar_sink_ref = []

    for i in range(SPLITTER_IO_SRC_MSG_COUNT):
        seed = randint(0,1)
        gen_io_request(splitter_io_src = splitter_io_src,
                       r_src = m_axi_r_src,
                       r_src_resp = m_axi_r_src_resp,
                       b_src_resp = m_axi_b_src_resp, 
                       splitter_io_src_val = splitter_io_src_val, 
                       r_src_val = m_axi_r_src_val,
                       b_src_val = m_axi_b_src_val, 
                       io_splitter_l_ack_sink = io_splitter_l_ack_sink_ref,
                       io_splitter_s_ack_sink = io_splitter_s_ack_sink_ref,
                       aw_sink =  m_axi_aw_sink_ref,
                       w_sink = m_axi_w_sink_ref,
                       w_sink_strb = m_axi_w_sink_strb_ref,
                       ar_sink = m_axi_ar_sink_ref,
                       is_load = (seed == 0))
    gen_data_file(splitter_io_src, "splitter_io_src", 64)
    gen_data_file(m_axi_r_src, "m_axi_r_src", 64)
    gen_data_file(m_axi_r_src_resp, "m_axi_r_src_resp", C_M_AXI_LITE_RESP_WIDTH)
    gen_data_file(m_axi_b_src_resp, "m_axi_b_src_resp", C_M_AXI_LITE_RESP_WIDTH)
    gen_data_file(splitter_io_src_val, "splitter_io_src_val", 1)
    gen_data_file(m_axi_r_src_val, "m_axi_r_src_val", 1)
    gen_data_file(m_axi_b_src_val, "m_axi_b_src_val", 1)
    gen_data_file(io_splitter_l_ack_sink_ref, "io_splitter_l_ack_sink_ref", 64)
    gen_data_file(io_splitter_s_ack_sink_ref, "io_splitter_s_ack_sink_ref", 64)
    gen_data_file(m_axi_aw_sink_ref, "m_axi_aw_sink_ref", 64)
    gen_data_file(m_axi_w_sink_ref, "m_axi_w_sink_ref", 64)
    gen_data_file(m_axi_w_sink_strb_ref, "m_axi_w_sink_strb_ref", 64/8)
    gen_data_file(m_axi_ar_sink_ref, "m_axi_ar_sink_ref", 64)

def gen_all_sink_ready():
    io_splitter_sink_rdy = gen_random_ready(TEST_LENGTH)
    m_axi_aw_sink_rdy = gen_random_ready(TEST_LENGTH)
    m_axi_w_sink_rdy = gen_random_ready(TEST_LENGTH)
    m_axi_ar_sink_rdy = gen_random_ready(TEST_LENGTH)
    
    gen_data_file(io_splitter_sink_rdy, "io_splitter_sink_rdy", 1)
    gen_data_file(m_axi_aw_sink_rdy, "m_axi_aw_sink_rdy", 1)
    gen_data_file(m_axi_w_sink_rdy, "m_axi_w_sink_rdy", 1)
    gen_data_file(m_axi_ar_sink_rdy, "m_axi_ar_sink_rdy", 1)
#--------------------------------------------------------
TEST_LENGTH = 2048
MSG_TYPE_LOAD_REQ = 31
MSG_TYPE_STORE_REQ = 2
MSG_TYPE_LOAD_MEM_ACK = 24
MSG_TYPE_NC_STORE_MEM_ACK = 27
MSG_TYPE_ERROR = 30
RAND_DELAY_FACTOR = 30
RAND_READY_FACTOR = 30
SPLITTER_IO_SRC_MSG_COUNT = 300
C_M_AXI_LITE_RESP_WIDTH = 2
AXI_LITE_RRESP_VALUE = 0
AXI_LITE_BRESP_VALUE = 0
MSG_DATA_SIZE_0B = 0
MSG_DATA_SIZE_1B = 1
MSG_DATA_SIZE_2B = 2
MSG_DATA_SIZE_4B = 3
MSG_DATA_SIZE_8B = 4

gen_spliter_io_axi_src()
gen_all_sink_ready()

