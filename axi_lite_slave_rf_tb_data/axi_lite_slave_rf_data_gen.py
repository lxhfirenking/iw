#!/usr/bin/python
import os
from random import randint

#!/usr/bin/python
import os
from random import randint

def gen_rand_msg(src, sinkref, val):
    msg_seed0 = randint(0, 0xffffffff)
    msg_seed1 = randint(0, 0xffffffff)
    data = "{0:032b}".format(msg_seed0) + "{0:032b}".format(msg_seed1)
    # convert to hex
    data = "{0:016x}".format(int(data, 2))
    src.append(data)
    sinkref.append(data)
    val.append("1")

# insert a random delay 
def gen_rand_delay_src(src, val, rand_delay_factor, src_resp = []):
    seed = randint(0, 99)
    while (seed < rand_delay_factor) :
        src.append("0")
        val.append("0")
        src_resp.append("0")
        seed = randint(0, 99)

# if the input array is shorter than length, then append it with delay/inval until the length match up
def add_delay_to(src_resp, val, length, src = []):
    while (len(src_resp) < length):
        src.append("0")
        src_resp.append("0")
        val.append("0")

#generate a ready array of length 
def gen_random_ready(length):
    tmp = []
    for i in range(length):
        seed = randint(0, 99)
        if (seed < RAND_READY_FACTOR):
            tmp.append("0")
        else:
            tmp.append("1")
    return tmp

#generate a data file from an array
def gen_data_file(array, unit_name, bit_length):
    s = '/%s.data.v' %(unit_name)
    f = open (filename(s), 'w');
    f.write("initial\nbegin\n")
    for i in range(len(array)):
        f.write("\t%s[%d] = %d'h%s;\n" %(unit_name, i, bit_length, array[i]))
    for i in range(len(array), TEST_LENGTH):
        f.write("\t%s[%d] = %d'h%d;\n" %(unit_name, i, bit_length, 0));
    f.write("end")
    f.close() 

#fullpath file name
#need to change...
def filename(s):
    return (os.environ["IOSPLITTER_AXI_LITE_ROOT"] + "/axi_lite_slave_rf_tb_data/" + s)


def gen_write_request(aw_src, aw_src_val, w_src, wstrb_src, w_src_val, bresp_sink, addr, data):
    gen_rand_delay_src(aw_src, aw_src_val, RAND_DELAY_FACTOR)
    gen_rand_delay_src(w_src, w_src_val, RAND_DELAY_FACTOR, src_resp = wstrb_src)
    aw_src.append("{0:016x}".format(addr))
    aw_src_val.append("1")
    w_src.append("{0:016x}".format(data))
    wstrb_src.append("01")
    w_src_val.append("1")
    bresp_sink.append("0")



def gen_read_request(ar_src, ar_src_val, r_sink, rresp_sink, addr, ref_data):
    gen_rand_delay_src(ar_src, ar_src_val, RAND_DELAY_FACTOR)
    ar_src.append("{0:016x}".format(addr))
    ar_src_val.append("1")
    r_sink.append("{0:016x}".format(ref_data))
    rresp_sink.append("0")

def gen_all_request():
    ref_data = []
    ref_addr = []
    for i in range(AXI_SLAVE_SRC_MSG_COUNT):
        msg = randint(0, 255)
        ref_addr.append(i)
        ref_data.append(msg)

    ar_src = []
    ar_src_val = []
    r_sink = []
    rresp_sink = []
    aw_src = []
    aw_src_val = []
    w_src = []
    w_src_val = []
    wstrb_src = []
    bresp_sink = []
    for i in range(AXI_SLAVE_SRC_MSG_COUNT):
        gen_write_request(aw_src, aw_src_val, w_src, wstrb_src, w_src_val, bresp_sink, ref_addr[i], ref_data[i])
        randaddr = randint(0, AXI_SLAVE_SRC_MSG_COUNT - 1)
        gen_read_request(ar_src, ar_src_val, r_sink, rresp_sink, randaddr, ref_data[randaddr])

    gen_data_file(ar_src, "s_axi_araddr_src", 64)
    gen_data_file(ar_src_val, "s_axi_arvalid_src", 1)
    gen_data_file(r_sink, "s_axi_rdata_sink_ref", 64)
    gen_data_file(rresp_sink, "s_axi_rresp_sink_ref", 2)

    gen_data_file(aw_src, "s_axi_awaddr_src", 64)
    gen_data_file(aw_src_val, "s_axi_awvalid_src", 1)
    gen_data_file(w_src, "s_axi_wdata_src", 64)
    gen_data_file(wstrb_src, "s_axi_wstrb_src", 8)

    gen_data_file(bresp_sink, "s_axi_bresp_sink_ref", 2)

def gen_all_sink_ready():
    r_sink_rdy = gen_random_ready(TEST_LENGTH)
    b_sink_rdy = gen_random_ready(TEST_LENGTH)
    gen_data_file(b_sink_rdy, "s_axi_bresp_sink_rdy")
    gen_data_file(r_sink_rdy, "s_axi_rdata_sink_rdy")


#--------------------------------------------------------------
RAND_DELAY_FACTOR = 30
RAND_READY_FACTOR = 30
AXI_SLAVE_SRC_MSG_COUNT = 200
TEST_LENGTH = 2048

gen_all_request()